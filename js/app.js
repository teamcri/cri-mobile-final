/* *********************************************************
CRI Mobile
Campus Recreation & Intramurals
Paul Bupe
 ********************************************************* */

/* *********************************************************
 Framework7 Init
 ********************************************************* */
// Initialize app
var myApp = new Framework7({
    fastClicks: true,
    precompileTemplates: true,
    sortable: false,
//    statusbarOverlay: true,
    swipePanel: 'left'
});

// Export selectors engine
var $$ = Dom7;

var mainView = myApp.addView('#view-main', {
    dynamicNavbar: true
});

var gfView = myApp.addView('#view-gf', {
    dynamicNavbar: true
});

var socialView = myApp.addView('#view-social', {
    dynamicNavbar: true
});

var facView = myApp.addView('#view-facilities', {
    dynamicNavbar: true
});

/*
// Pull to refresh content
var ptrContent = $('.pull-to-refresh-content');

// Add 'refresh' listener on it
ptrContent.on('refresh', function (e) {

    // Emulate 2s loading
    setTimeout(function () {
        // When loading done, we need to reset it
        server.getSocial(1);
        storage.setScrollIndex(1);
        myApp.pullToRefreshDone();
    }, 1000);
});
*/


/* *********************************************************
 Main App Init
 ********************************************************* */
var app = {
    // Constructor
    initialize: function() {
        this.bindEvents();
        initialize();
    },

    // Bind event listeners
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        $$('.infinite-scroll').on('infinite', socialInfiniteLoad);
        $$('.event-feed').on('infinite', eventInfiniteLoad);

    },

    // Device is ready
    onDeviceReady: function() {
        navigator.splashscreen.hide();

        initPushwoosh();

        if (window.device.platform === 'iOS' && parseFloat(window.device.version) === 7.0) {
            StatusBar.overlaysWebView(false);
            StatusBar.styleLightContent();
        }
        // Google Analytics Plugin
        gaPlugin = window.plugins.gaPlugin;

        if ($.jStorage.get("tracking")) {
            gaPlugin.init(nativePluginResultHandler, nativePluginErrorHandler, "UA-54192829-1", 10);
        } else {
            if (($.jStorage.get("tracking") != false)) {
                navigator.notification.confirm('CRI Mobile would like your permission to collect usage statistics. No personal or user identifiable data will be collected.', permissionCallback, 'Attention', 'Allow,Deny');
            }
        }

        app.receivedEvent('deviceready');
    },

    receivedEvent: function(id) {

/*        //set push notifications handler
        document.addEventListener('push-notification',
            function(event)
            {
                console.log("Push");
                myApp.alert(JSON.stringify(event));
                var title = event.notification.title;
                var userData = event.notification.userdata;

                //dump custom data to the console if it exists
                if(typeof(userData) != "undefined") {
                    myApp.addNotification({
                        title: title,
                        message: userData,
                        media: '<i class="icon icon-cri-leaf"></i>'
                    });

                }


                //and show alert
                //alert(title);

                //stopping geopushes
                //pushNotification.stopGeoPushes();
            }
        );*/
    }
};





<!-- handlebars -m templates/> templates.js -->

//https://gsucampusrec:@bitbucket.org/teamcri/cri-mobile.git


function initialize() {

    //This function will only ever run once unless the storage is deleted.
    if(storage.getFirstRun()) {
        storage.setReminderState(true);
        storage.setPushState(true);
        storage.setFirstRun(false);
    }
    reminderStatus();
    pushStatus();

    myApp.initImagesLazyLoad($$('.social-feed'));

    //Process the initial calendar filters

    var initialCalendars = ["aquatics", "club_sports", "fitness","golf", "intramural_sports", "southern_adventures", "university_wellness", "gsu_campusrec"]; //To Do: Add "shooting_sports_edu",
    var getInitialFilter = storage.getFilters() || [];
    var initialFilter = (getInitialFilter.length > 0) ? getInitialFilter : initialCalendars;
    storage.setFilters(initialFilter);
    server.getSocial(1);
    server.getHours();

    if($.jStorage.getTTL("events") > 0 && !_.isEmpty(storage.getEvents())) {
        myApp.showPreloader();
        var events = storage.getEvents();
        $("#view-main").find('.page-content').html(loader.loadMain(events));
        myApp.hidePreloader();
    } else {
//        console.log("no events");
        server.getEvents();
    }

    if($.jStorage.getTTL("gf") > 1 && !_.isEmpty(storage.getGF())) {

        var gfevents = storage.getGF();
        $("#view-gf").find('.page-content').html(loader.loadGF(gfevents));
    } else {
//        console.log("no gf");
        server.getGroupFit();
    }

    //var social = storage.getSocial();
    //$("#view-social").find('.page-content ul').html(sourceTemplate(social));


    var filterList = $('#defaults input');
    for ( var i=0; i < filterList.length; i++) {
        if(checkfilter(filterList[i].value)) {
            filterList[i].checked=true;
        }
    }

    // Handle the calendar filters
    $$('input[type="checkbox"]').on('change', ".eFilter", function(){

        var formData = myApp.formToJSON('#defaults');
        var values = _.values(formData);
        values = _.flatten(values);

//            navigator.notification.alert(JSON.stringify(values), function(){}, "", "");
//            myApp.alert(JSON.stringify(values));
        storage.setFilters(values);
        reload();
//    rebind();
//    console.log(values);
//    console.log(JSON.stringify(values));

    });

    //Handle local reminders
    $$('input[type="checkbox"]').on('change', ".reminder-switch", function(){
        if(this.checked) {
            storage.setReminderState(true);
        } else {
            storage.setReminderState(false);
            notifications.cancelAll();
            $.jStorage.set("myEvents", []);
            reload();
        }

    });

    //Handle Push settings
    $$('input[type="checkbox"]').on('change', ".push-switch", function(){
        if(this.checked) {
            storage.setPushState(true);
            window.plugins.toast.showShortBottom("Push Notifications Enabled", function(a){}, function(b){});
            initPushwoosh();
        } else {
            storage.setPushState(false);
            navigator.notification.alert("Push notifications have been disabled.", function(){}, "Warning", "");
            initPushwoosh();
        }
    });

    $$(document).on('click', '.reload', function(){
        //console.log("Reload clicked");
        server.getEvents();
        server.getGroupFit();
    });

/*    $$(document).on('push-notification', function(event){
        console.log("Push");
        myApp.alert(JSON.stringify(event));
        var title = event.notification.title;
        var userData = event.notification.userdata;

        //dump custom data to the console if it exists
        if(typeof(userData) != "undefined") {
            myApp.addNotification({
                title: title,
                message: userData,
                media: '<i class="icon icon-cri-leaf"></i>'
            });

        }
    });*/

/*    $$(document).on('click', '#view-social', function(){
     //console.log("Reload clicked");
     console.log("Social");
     server.getSocial(1);
     });*/

    $$(document).on('click', '.navbar', function(){
        $$('.page-content').scrollTop(0, 200);
    });

    $$(document).on('click', '.tab-link', function() {
        loader.updateHours();
    });

//    var timeString = moment(event.start_time).utc().format("MM/DD/YYYY hh:mm:ss a");


    $$('.event-feed, .gf-feed').on('click', '.subscribe', function(){

        //Ask for permission
        cordova.plugins.notification.local.registerPermission(function (granted) {
        });
        //window.plugin.notification.local.promptForPermission();
        //console.log("Reminder click event");
        if(storage.getReminderState()) {

            var span = $(this).find('i');
            var id = $($(this).parents().get(0)).attr('id');
            var name = $($(this).parents().get(0)).attr("data-name");
            var time = $($(this).parents().get(0)).attr("data-time");
            time = new Date(time);
            //console.log(span);
            if (moment(time) < moment().add("1","hours")) {
                window.plugins.toast.showShortBottom("Can't set reminder for this event.", function(a){}, function(b){});
            } else {
                span.toggleClass("alarm");
                var alarm = {};
                alarm.id = id;
                alarm.title = name;
                alarm.date = moment(time).subtract("1","hours").toDate();
                alarm.time = moment(time).format('h:mm A');
                storage.addEvent(alarm);

                if($.jStorage.get("tracking")) {
                    gaPlugin.trackEvent(nativePluginResultHandler, nativePluginErrorHandler, "Button", "Click", name, 1);
                }
            }

        } else {
            navigator.notification.alert("Reminders are disabled in Settings.", function(){}, "Can't Set Reminder", "");
        }

    });

}

/* *********************************************************
 General Functions
 ********************************************************* */

function reminderStatus() {
    var reminderSwitch = $$(".label-switch").find("input");
    if(storage.getReminderState()) {
        reminderSwitch.prop('checked', true);

    } else {
//        console.log(false);
        reminderSwitch.prop('checked', false);
    }
}

function pushStatus() {
    var pushSwitch = $$(".push-switch").find("input");
    if(storage.getPushState()) {
        pushSwitch.prop('checked', true);

    } else {
        pushSwitch.prop('checked', false);
    }
}

function initPushwoosh() {
    var pushNotification = window.plugins.pushNotification;
    if(device.platform == "Android")
    {
        registerPushwooshAndroid();
    }

    if(device.platform == "iPhone" || device.platform == "iOS")
    {
        registerPushwooshIOS();
    }
}

function reload() {
    var mainEvents = $("#view-main");
    var gfEvents = $("#view-gf");

    if($.jStorage.getTTL("events") > 0 && !_.isEmpty(storage.getEvents())) {
        var events = storage.getEvents();
        mainEvents.find('.page-content').html(loader.loadMain(events));
    } else {
//        console.log("no events");
        server.getEvents();
    }

    if($.jStorage.getTTL("gf") > 1 && !_.isEmpty(storage.getGF())) {
        var gfevents = storage.getGF();
        gfEvents.find('.page-content').html(loader.loadGF(gfevents));
    } else {
//        console.log("no gf");
        server.getGroupFit();
    }

}


/* *********************************************************
 Storage Class
 ********************************************************* */

var storage = {

    flush: function() {
        $.jStorage.flush();
    },

    setScrollIndex: function(int) {
        $.jStorage.set("scrollIndex", int);
    },

    getScrollIndex: function() {
        return $.jStorage.get("scrollIndex", 1);
    },

    setFeedIndex: function(int) {
        $.jStorage.set("feedIndex", int);
    },

    getFeedIndex: function() {
        return $.jStorage.get("feedIndex");
    },

    setFirstRun: function(state) {
        $.jStorage.set("firstRun", state)
    },
    getFirstRun: function () {
        return $.jStorage.get("firstRun", true);
    },
    getFilters: function() {
        return $.jStorage.get("filter");
    },

    setFilters: function(filters) {
        $.jStorage.set("filter", filters);
    },

    setReminderState: function(state) {
        $.jStorage.set("reminder", state);
    },

    getReminderState: function() {
        return $.jStorage.get("reminder", 0);
    },

    setPushState: function(state) {
        $.jStorage.set("pushState", state);
    },

    getPushState: function() {
        return $.jStorage.get("pushState", 0);
    },

    getEvents: function() {
        return $.jStorage.get("events", []);
    },

    putEvents: function(events) {
        $.jStorage.set("events", events, {TTL: 3600000});
    },

    getHours: function() {
        return $.jStorage.get("hours", {});
    },

    putHours: function(hours) {
        $.jStorage.set("hours", hours, {TTL: 86400000});
    },
    putSocial: function(social) {
        $.jStorage.set("social", social);
    },

    putGF: function(events) {
        $.jStorage.set("gf", events, {TTL: 3600000});
    },

    getGF: function() {
        return $.jStorage.get("gf", []);
    },

    getSocial: function() {
        return $.jStorage.get("social", []);
    },

    getMyEvents: function() {
        return $.jStorage.get("myEvents", [])
    },

    putMyEvents: function(events) {
        var tempEvents = this.getMyEvents();
        tempEvents.push(events);
        $.jStorage.set("myEvents", tempEvents);
//        console.log("Event Added");
    },

    removeEvent: function(eventid) {
        var eventIDs = this.getMyEvents();
//        console.log(eventIDs);
        var reduced = _.reject(eventIDs, function(id){ return id.id == eventid.id; });
        if (!_.isEmpty(reduced)) {
            $.jStorage.set("myEvents", reduced);
        }
        if (_.isEmpty(reduced)) {
            $.jStorage.set("myEvents", []);
        }

//        console.log(this.getMyEvents());

    },

    addEvent: function(event) {
        var eventIDs = this.getMyEvents();

        if (_.some(eventIDs, function(obj){ return obj.id == event.id})) {
//            console.log("exists");
            this.removeEvent(event);
            notifications.remove(event)
        } else {
//            console.log("doesn't exist");
            this.putMyEvents(event);
            notifications.add(event);
//            console.log(event.time)
        }

    }
};


/* *********************************************************
 Notification Class
 ********************************************************* */
var notifications = {

    add: function(event){

        cordova.plugins.notification.local.schedule({
            id:         event.id,  // A unique id of the notifiction
            at:       event.date,    // This expects a date object
            text:    event.title,  // The message that is displayed
            title:      "CRI Event: " + event.time,  // The title of the message
            autoCancel: true // Setting this flag and the notification is automatically canceled when the user clicks it
        });

        cordova.plugins.notification.local.on("schedule", function(){
            window.plugins.toast.showShortBottom('1 hour reminder set for "' + event.title + '"', function(a){}, function(b){});
        });

/*        window.plugin.notification.local.onadd = function (id, state, json) {
            window.plugins.toast.showShortBottom('1 hour reminder set for "' + event.title + '"', function(a){}, function(b){});
//            navigator.notification.alert('1 hour reminder set for "' + event.title + '"', function(){}, "Event Reminder Set!", "");
        };*/
    },

    remove: function(event){

        cordova.plugins.notification.local.cancel(event.id, function () {});
        window.plugins.toast.showShortBottom('Event Reminder has been cancelled', function(a){}, function(b){});
//        navigator.notification.alert("Event reminder has been cancelled", function(){}, "Reminder Cancelled", "");
    },

    cancelAll: function(){

        cordova.plugins.notification.local.cancelAll(function() {
            navigator.notification.alert("Event reminders have been disabled.", function(){}, "Warning", "");
        }, this);

    },

    isScheduled: function(id) {
        var response;

        cordova.plugins.notification.local.isScheduled(id, function (isScheduled) {
            return isScheduled;
        });

    }

};


/* ********************************************************
Template Setup
 ********************************************************* */

var eventList;
var eventCount = _.size(storage.getEvents());
function checkfilter(text){
    var list = $.jStorage.get("filter");
    var returnVal = false;
    for (var i = 0; i < list.length; i++) {
        if(RegExp( '\\b' + list[i] + '\\b', 'i').test(text.toLowerCase())){
            returnVal = true;
            break;
        }
    }
    return returnVal;
}


var loadingContent = '<div class="col-25 loader"><h3>Loading...</h3><br><br><span class="preloader"></span></div>';
var refreshContent = '<div class="loading"><i class="fa fa-refresh fa-spin fa-2x"></i></div>';
var errorContent = '<div class="error"><br/><br/><h3>Connection Error</h3><br/><a class="btn btn-default reload">Reload</a></div>';
var NoContent = '<div class="error"><br/><br/><h3>No Content</h3><br/><a class="btn btn-default reload">Reload</a></div>';


/* *********************************************************
 Event Loader Class
 ********************************************************* */

var loader = {
    loadMyEvents: function(events){
        var myEventFeed = $(".my-event-feed");
        myEventFeed.html("");

        events = filterMyEvents(events);

        var grouped = groupEvents(events);

        for (var i=0; i<grouped.length; i++) {
            myEventFeed.append('<div class="date">'+ grouped[i][0].fullDate +'</div>')
            //var html = newTemplate(grouped[i]);
            var html = Template7.templates.eventT(grouped[i]);
            myEventFeed.append(html);
        }
    },

    loadMain: function(events) {
        events = filterEvents(events);
        var grouped = groupEvents(events);
        var builtevents = "";

        for (var i=0; i<grouped.length; i++) {
            builtevents += '<div class="date">'+ grouped[i][0].fullDate +'</div>';
            var html = myApp.templates.eventTemplate(grouped[i]);
            builtevents += html;
        }

        if (_.isEmpty(builtevents) || _.isNull(builtevents)) {
            return NoContent;
        } else {
            return builtevents;
        }
    },

    loadGF: function(events) {

        events = filterEvents(events, ["group_fitness"]);
        var grouped = groupEvents(events);
        var builtevents = "";
        for (var i=0; i<grouped.length; i++) {
            builtevents += '<div class="date">'+ grouped[i][0].fullDate +'</div>';
            var html = myApp.templates.eventTemplate(grouped[i]);
            builtevents += html;
        }

        if (_.isEmpty(builtevents) || _.isNull(builtevents)) {
            return NoContent;
        } else {
            return builtevents;
        }
    },

    updateHours: function() {
        var hoursDiv = $("#hours-status");
        var hoursbar = $(".navbar");
        var hours = storage.getHours();

        if(!_.isNull(hours)){

            var today = moment().format("L");
            var todayObject = _.pick(hours, today);
            todayObject = todayObject[today];
            var todayOpen = todayObject.open;
            if(todayObject.open == "Closed") {
                //console.log("Closed");
                hoursDiv.html("The RAC is Currently Closed");
                hoursDiv.attr('class', 'closed');
            } else {

                var open = moment(todayObject.open, "h a");

                if(todayObject.closed == "12 a.m.") {
                    /*
                     Compensate for midnight
                     */
                    var close = moment(todayObject.closed, "h a");
                    close.add(1, "days");
                } else {
                    var close = moment(todayObject.closed, "h a");
                }
                var midnight = moment("12 a.m.", "h a");
                if( (moment().isSame(open) || moment().isAfter(open)) && (moment().isBefore(close)) ) {
                    hoursDiv.html("The RAC is Currently Open");
                    hoursDiv.attr('class', 'open');
                    //hoursbar.css("border-bottom", "3px solid red");

                } else {
                    hoursDiv.html("The RAC is Currently Closed");
                    hoursDiv.attr('class', 'closed');
                    //hoursbar.css("border-bottom", "3px solid red");

                }
            }

        }
    }
};


/*{"start_time" : {"$gte":"2014-07-28T10:05:04-04:00"}}
{"$and": [ {"gCal" : "group_fitness"}, { "start_time" : "2014-07-28T10:05:04-04:00" } ] }
{"$and": [ {"gCal" : {"$ne":"group_fitness"}}, { "start_time" : {"$gte":"2014-07-28T10:05:04-04:00"} } ] }*/

var today = (moment("0","hh").subtract("4", "hours")).format(); //Compensating for EDT

/* *********************************************************
 Server Class
 ********************************************************* */

var server = {

    getEvents: function() {

        //var eventCount = _.size(storage.getEvents());
        //console.log(eventCount);

        if($.jStorage.getTTL("events") == 0 || isEmpty($.jStorage.get("events"))) {
            myApp.showPreloader();

                $.ajax({
                "url": "https://api.cloud.appcelerator.com/v1/events/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE&response_json_depth=1&order=start_time&new_pagination=true&count=true&skip=0&limit=50&where=%7B%22%24and%22%3A%20%5B%20%7B%22gCal%22%20%3A%20%7B%22%24ne%22%3A%22group_fitness%22%7D%7D%2C%20%7B%20%22start_time%22%20%3A%20%7B%22%24gte%22%3A%22" + today + "%22%7D%20%7D%20%5D%20%7D",
                "dataType": "json",
                "success": function (incoming) {
                    //console.log(incoming);
                    storage.setFeedIndex(incoming.meta["count"]);
//                console.log("Received Events")
                    eventList = _.map(incoming.response.events, createEventObject);
                    storage.putEvents(eventList);
                    eventCount = _.size(storage.getEvents());
                    $("#view-main").find('.page-content').html(loader.loadMain(eventList));
                    myApp.hidePreloader();
                }, //End success function
                error: function (request, status, error) {
//                console.log("Event Get Error");
                    $.jStorage.set("events", [], {TTL: 1});
                    myApp.hidePreloader();
                    $("#view-main").find('.page-content').html(errorContent);
                }
            });

        }
    },

    getHours: function() {
        if($.jStorage.getTTL("hours") == 0 || isEmpty($.jStorage.get("hours"))) {
            $.ajax({
                "url": "https://api.cloud.appcelerator.com/v1/objects/hours14/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE",
                "dataType": "json",
                "success": function (incoming) {
                    var hours = incoming.response.hours14[0];
                    hours = _.omit(hours, ["id","created_at","updated_at"]);
                    storage.putHours(hours);
                }, //End success function
                error: function (request, status, error) {

                }
            });
        }
    },

    getGroupFit: function() {
        if($.jStorage.getTTL("gf") == 0 || isEmpty($.jStorage.get("gf"))) {
            $(".gf-feed").html(loadingContent);
//        $(".gf-feed").html("testing");
//        $("#view-gf").find('.page-content').html(loadingContent);
//        console.log("reloading");
            $.ajax({
                "url": "https://api.cloud.appcelerator.com/v1/events/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE&response_json_depth=1&order=start_time&per_page=50&where=%7B%22%24and%22%3A%20%5B%20%7B%22gCal%22%20%3A%20%22group_fitness%22%7D%2C%20%7B%20%22start_time%22%20%3A%20%7B%22%24gte%22%3A%22" + today + "%22%7D%20%7D%20%5D%20%7D",
                "dataType": "json",
                "success": function (incoming) {
//                console.log("Received Events")
                    eventList = _.map(incoming.response.events, createEventObject);
                    storage.putGF(eventList);
//                console.log(incoming);
                    $("#view-gf").find('.page-content').html(loader.loadGF(eventList));

                }, //End success function
                error: function (request, status, error) {

                    $.jStorage.set("gf", [], {TTL: 1});
//                console.log("Event Get Error");
                    $("#view-gf").find('.page-content').html(errorContent);


                }
            });

        }
    },


    getSocial: function(page) {
        $(".social-feed > ul").html(loadingContent);
        console.log('Starting getSocial()...');
        $.ajax({
            "url":"https://raclife-stream.firebaseio.com/posts.json",
            //"url": "https://api.cloud.appcelerator.com/v1/posts/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE&response_json_depth=1&per_page=15&page=" + page,
            "dataType": "json",
            "success": function (incoming) {
                var posts = [];
                $.each(incoming, function(index, value){
                    posts.push(value);
                });
                posts.reverse();
                var socialFeed = posts.map(function applyTemplate(content) {
                    if (content.platform == "twitter") {
                        return {
                            created_at: moment(new Date(content.timestamp)).format('MMMM Do, YYYY, h:mm A'),
                            imageUrl: content.profileImg,
                            media: content.media,
                            name: content.name,
                            screen_name: content.username,
                            text: content.text,
                            url: "http://www.GSUCampusRec.com", //content.url,
                            twitter: true
                        };

                    }

                    if (content.platform == "instagram") {

                        return {
                            created_at: moment(new Date(content.timestamp)).format('MMMM Do, YYYY, h:mm A'),
                            imageUrl: content.profileImg,
                            media: content.media,
                            name: content.name,
                            screen_name: content.username,
                            text: content.text,
                            url: "http://www.GSUCampusRec.com", //content.url,
                            instagram: true
                        };

                    }
                });

                $("#view-social").find('.page-content ul').html(myApp.templates.socialTemplate(socialFeed));

                console.log("socialFeed: ");
                console.log(socialFeed);
                // create an array of objects
                return socialFeed;
//            socialFeed = sourceTemplate(socialFeed);
//            $(".social-feedback").html("");
//            $(".social-feed").html(socialFeed);

            }, //End success function
            error: function (request, status, error) {

                $("#view-social").find('.page-content ul').empty();
                $("#view-social").find('.page-content ul').html(errorContent);

            }
        });
    }
    

    /*
    // Original getSocial script
    getSocial: function(page) {
        $(".social-feed > ul").html(loadingContent);
        $.ajax({
            "url": "https://api.cloud.appcelerator.com/v1/posts/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE&response_json_depth=1&per_page=15&page=" + page,
            "dataType": "json",
            "success": function (incoming) {

                var data = incoming.response.posts;
                console.log("'data = incoming.response.posts' = ");console.log(data);
                var socialFeed = data.map(function applyTemplate(content) {
                    var temp;
                    console.log("'content.custom_fields' = ");console.log(content.custom_fields);
                    if (content.content == "twitter") {

                        return {
                            created_at: moment(content.custom_fields.created_at).format('MMMM Do, YYYY, h:mm A'),
                            imageUrl: content.custom_fields.imageUrl,
                            media: content.custom_fields.media,
                            name: content.custom_fields.name,
                            screen_name: content.custom_fields.username,
                            text: content.custom_fields.text,
                            url: content.custom_fields.url,
                            twitter: true
                        };

                    }

                    if (content.content == "instagram") {

                        return {
                            created_at: moment(content.custom_fields.created_at).format('MMMM Do, YYYY, h:mm A'),
                            imageUrl: content.custom_fields.imageUrl,
                            media: content.custom_fields.media,
                            name: content.custom_fields.name,
                            screen_name: content.custom_fields.username,
                            text: content.custom_fields.text,
                            url: content.custom_fields.url,
                            instagram: true
                        };

                    }
                });

                $("#view-social").find('.page-content ul').html(myApp.templates.socialTemplate(socialFeed));
                console.log("socialFeed: ");
                console.log(socialFeed);
                return socialFeed;
//            socialFeed = sourceTemplate(socialFeed);
//            $(".social-feedback").html("");
//            $(".social-feed").html(socialFeed);

            }, //End success function
            error: function (request, status, error) {

                $("#view-social").find('.page-content ul').empty();
                $("#view-social").find('.page-content ul').html(errorContent);

            }
        });
    }
    */
};


/* *********************************************************
 Helper Functions
 ********************************************************* */


function createEventObject(event) {
    var date = moment(event.start_time).utc().format('MMMM Do, YYYY');
    var fullDate = moment(event.start_time).utc().format('dddd, MMMM Do, YYYY');
    //var fullDate = moment(event.start_time).utc().format('dddd, MMMM Do, YYYY');
    var time = moment(event.start_time).utc().format('h:mm A');
    var day = moment(event.start_time).utc().format('dddd');
    var timeString = moment(event.start_time).utc().format("MM/DD/YYYY hh:mm:ss a");
    var sortString = moment(event.start_time).utc();
    //console.log(day);

    var newObject = {};
    newObject.id = (event.id).replace(/\D/g,''); //remove letters from ID
    newObject.eventTime = time;
    newObject.timeString = timeString;
    newObject.sortString = sortString;
    newObject.eventDate = date;
    newObject.eventDay = day;
    newObject.fullDate = fullDate;
    newObject.eventTitle = event.name;
    newObject.eventLocation = validate(event.custom_fields.location);
    newObject.eventCal = validate(event.custom_fields.gCal);
    newObject.eventDescription = validate(event.details);

    if(!_.isNull(event.custom_fields.image_url)) {
        newObject.pictureLink = event.custom_fields.image_url;
    }

    return newObject;
}

function filterEvents(events, list){
    var filter = $.jStorage.get("filter");
    list = typeof list !== 'undefined' ? list : filter;
    var filtered =  _.filter(events, function(event){
        return _.contains(list, event.eventCal)
    });
    return setIcons(filtered);
}

function filterMyEvents(events){
    var myEvents = storage.getMyEvents();
    return  _.filter(events, function(event){
        return _.contains(myEvents, event.id)
    })
}

function groupEvents(events) {

//    console.log(grouped);
    var sorted =_.sortBy(events, function(event){
//        console.log(event);
        return event.sortString;
    });

    var grouped = _.groupBy(sorted, function(event){
//        console.log(event.fullDate);
        return event.eventDate;
    });

//    console.log(grouped);
    return _.toArray(grouped);

}


function setIcons(events) {

    var savedEvents = storage.getMyEvents();
    return _.each(events, function(event){
        _.omit(event, "isSaved");

        if(_.some(savedEvents, function(obj){ return obj.id == event.id})){
            event["isSaved"] = true;
            return event;
        } else {
            return event;
        }
    })
}





//Check if value is empty
function isEmpty(value){
    return (typeof value === "undefined" || value === null);
}

//If empty, return empty "" instead of error
function validate(value){
    return isEmpty(value) ? "" : value;
}

function filterCalendar(data) {

    return $.map(data, function(event) {
        if(checkfilter(event.eventCal)) {
            return event;
        }
    });
}


/* *********************************************************
 Infinite scrolling
 ********************************************************* */

var feedLoading = false;
var feedMaxItems = 100;
//var eventCount = _.size(storage.getEvents());
//console.log(eventCount);

function eventInfiniteLoad(){
    //var feedLoading = false;
    var feedMaxItems = 100;

    if(feedLoading) return;
    feedLoading = true;
    myApp.showIndicator();
    maxItems = storage.getFeedIndex();

    setTimeout(function () {
        feedLoading = false;

        if (eventCount >= maxItems) {
            //console.log("done");
            // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
            myApp.hideIndicator();
            myApp.detachInfiniteScroll($$('.event-feed'));
            // Remove preloader
            //$$('.infinite-scroll-preloader').remove();
            return;
        }
        $.ajax({
            "url": "https://api.cloud.appcelerator.com/v1/events/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE&response_json_depth=1&order=start_time&new_pagination=true&count=true&skip=" + eventCount + "&limit=50&where=%7B%22%24and%22%3A%20%5B%20%7B%22gCal%22%20%3A%20%7B%22%24ne%22%3A%22group_fitness%22%7D%7D%2C%20%7B%20%22start_time%22%20%3A%20%7B%22%24gte%22%3A%22" + today + "%22%7D%20%7D%20%5D%20%7D",
            "dataType": "json",
            "success": function (incoming) {
//                console.log("Received Events")
                eventList = _.map(incoming.response.events, createEventObject);
                var thisCount = _.size(eventList);
                //console.log(thisCount);
                //console.log(eventCount);
                //console.log(eventCount + thisCount);
                eventCount += thisCount;
                //console.log(eventCount);

                myApp.hideIndicator();
                $("#view-main").find('.page-content').append(loader.loadMain(eventList));
            }, //End success function
            error: function (request, status, error) {
                myApp.hideIndicator();
//                console.log("Event Get Error");
               // $.jStorage.set("events", [], {TTL: 1});
               //$("#view-main").find('.page-content').html(errorContent);
            }
        });


    }, 1000);

}










/* ----------- */

// Loading flag
var loading = false;

// Last loaded index
// Max items to load
var maxItems = 300;
storage.setScrollIndex(15);

function socialInfiniteLoad() {
    // Exit, if loading in progress
    if (loading) return;

    // Set loading flag
    loading = true;

    // Emulate 1s loading
    setTimeout(function () {
        var lastIndex = storage.getScrollIndex();

        // Reset loading flag
        loading = false;

        if (lastIndex >= maxItems) {
            // Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
            myApp.detachInfiniteScroll($$('.infinite-scroll'));
            // Remove preloader
            $$('.infinite-scroll-preloader').remove();
            return;
        }

        // Update last loaded index
        lastIndex+=15;

        // Very inefficient -- needs to be changed to use callbacks
       // https://api.cloud.appcelerator.com/v1/posts/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE&response_json_depth=1&limit=15&skip=0&new_pagination=true
        $.ajax({
            "url": "https://api.cloud.appcelerator.com/v1/posts/query.json?key=cHh9DGat3bUdWqvKFUxxH62lAV1yyUSE&response_json_depth=1&limit=15&new_pagination=true&skip=" + lastIndex,
            "dataType": "json",
            "success": function (incoming) {

                var data = incoming.response.posts;
                //console.log(data);
                var socialFeed = data.map(function applyTemplate(content) {
                    var temp;
                    //console.log(content.custom_fields);
                    if (content.content == "twitter") {

                        var date = moment(content.custom_fields.created_at).format('MMMM Do, YYYY, h:mm A');

                        return {
                            created_at: date,
                            imageUrl: content.custom_fields.imageUrl,
                            media: content.custom_fields.media,
                            name: content.custom_fields.name,
                            screen_name: content.custom_fields.username,
                            text: content.custom_fields.text,
                            url: content.custom_fields.url,
                            twitter: true
                        };

                    }
                    if (content.content == "instagram") {

                        var date = moment(content.custom_fields.created_at).format('MMMM Do, YYYY, h:mm A');

                        return {
                            created_at: date,
                            imageUrl: content.custom_fields.imageUrl,
                            media: content.custom_fields.media,
                            name: content.custom_fields.name,
                            screen_name: content.custom_fields.username,
                            text: content.custom_fields.text,
                            url: content.custom_fields.url,
                            instagram: true
                        };


                    }
                });

                $("#view-social").find('.page-content ul').append(myApp.templates.socialTemplate(socialFeed));

            }, //End success function
            error: function (request, status, error) {

                $$("#view-social").find('.page-content ul').empty();
                $$("#view-social").find('.page-content ul').html(errorContent);

            }
        });

        //$$('.social-list').data("socialPage", lastIndex);
        storage.setScrollIndex(lastIndex);

    }, 1000);

}

/* *********************************************************
 Analytics Functions
 ********************************************************* */


function homeEvent() {
    if($.jStorage.get("tracking")){
        gaPlugin.trackPage( nativePluginResultHandler, nativePluginErrorHandler, "Home Page");
    }
    }

function gfEvent() {
    if($.jStorage.get("tracking")){
        gaPlugin.trackPage( nativePluginResultHandler, nativePluginErrorHandler, "Group Fit Page");
    }
 }

function socialEvent() {
    if($.jStorage.get("tracking")){
        gaPlugin.trackPage( nativePluginResultHandler, nativePluginErrorHandler, "Social Page");
    }
}

function facilityEvent() {
    if($.jStorage.get("tracking")){
        gaPlugin.trackPage( nativePluginResultHandler, nativePluginErrorHandler, "Facility Page");
    }
}

function mainLineEvent() {
    if($.jStorage.get("tracking")) {
        gaPlugin.trackEvent(nativePluginResultHandler, nativePluginErrorHandler, "Button", "Call", "Main Line", 1);
    }
}

function SAEvent() {
    if($.jStorage.get("tracking")) {
        gaPlugin.trackEvent(nativePluginResultHandler, nativePluginErrorHandler, "Button", "Call", "Southern Adventures", 1);
    }
}

function golfEvent() {
    if($.jStorage.get("tracking")) {
        gaPlugin.trackEvent(nativePluginResultHandler, nativePluginErrorHandler, "Button", "Call", "Golf", 1);
    }
}
function ssecEvent() {
    if($.jStorage.get("tracking")) {
        gaPlugin.trackEvent(nativePluginResultHandler, nativePluginErrorHandler, "Button", "Call", "SSEC", 1);
    }
}

function weatherEvent() {
    if($.jStorage.get("tracking")) {
        gaPlugin.trackEvent(nativePluginResultHandler, nativePluginErrorHandler, "Button", "Call", "Weather Line", 1);
    }
}

//Analytics permission check

function permissionCallback (button) {
    if (button === 1) {
        gaPlugin.init(nativePluginResultHandler, nativePluginErrorHandler, "UA-54192829-1", 10);
        gaPlugin.trackEvent(nativePluginResultHandler, nativePluginErrorHandler, "App", "Open", "App", 1);
        $.jStorage.set("tracking", true);
    }
    if (button === 2) {
        $.jStorage.set("tracking", false);

    }
}

function nativePluginResultHandler (result) {
    //alert('nativePluginResultHandler - '+result);
    //console.log('nativePluginResultHandler: '+result);
//    window.plugins.toast.showLongBottom(result, function(a){}, function(b){});
}

function nativePluginErrorHandler (error) {
    //alert('nativePluginErrorHandler - '+error);
    //console.log('nativePluginErrorHandler: '+error);
//    window.plugins.toast.showLongBottom(result, function(a){}, function(b){});
}

function goingAway() {
    gaPlugin.exit(nativePluginResultHandler, nativePluginErrorHandler);
}

/* *********************************************************
CRI Mobile - Quick Fix for new Social Stream
Campus Recreation & Intramurals
Kevin Kips

Description: This section was added as a quick fix for the broken Social tab of the app. The Appcelerator db has broken and we've now switched over to Google's Firebase. Please see w3.georgiasouthern.edu/cri/tagboard for a working example. The way this quick fix works is: Everything with the 'angular' prefix has been added because that's what was used for the tagboard (little experience with Framework7 at the moment). When the new Web GA finishes tutorials on Framework7, please translate all AngularJS code into Framework7 code.
 ********************************************************* */

/* *********************************************************
 AngularJS Init
 ********************************************************* */
// Initialize Angular apps
