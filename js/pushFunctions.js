/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

function registerPushwooshAndroid() {

    var pushNotification = window.plugins.pushNotification;


    //set push notifications handler
    document.addEventListener('push-notification',
        function(event)
        {

            /*

             bubbles: true
             cancelBubble: false
             cancelable: true
             clipboardData: undefined
             currentTarget: null
             defaultPrevented: false
             eventPhase: 0
             notification: Object
                collapse_key: "do_not_collapse"
                foreground: true
                from: "746865478311"
                header: "test"
                onStart: false
                p: "c"
                title: "testtrst"
                vib: "0"
                __proto__: —
             returnValue: true
             srcElement: HTMLDocument
             target: HTMLDocument
             timeStamp: 1425052421059
             type: "push-notification"
             __proto__: —


             */


            console.log(event);
            //myApp.alert(JSON.stringify(event));
            var title = event.notification.header;
            var userData = event.notification.title;

            //dump custom data to the console if it exists
            if(typeof(userData) != "undefined") {
                myApp.addNotification({
                    title: title,
                    message: userData,
                    media: '<i class="icon icon-cri-leaf"></i>'
                });

            }

            //and show alert
            //alert(title);

            //stopping geopushes
            //pushNotification.stopGeoPushes();
        }
    );

    //initialize Pushwoosh with projectid: "GOOGLE_PROJECT_ID", appid : "PUSHWOOSH_APP_ID". This will trigger all pending push notifications on start.
    pushNotification.onDeviceReady({ projectid: "746865478311", appid : "FA47B-C5156" });

    // If push notifications are enabled in user settings
    if(storage.getPushState()) {

        //register for push notifications
        pushNotification.registerDevice(
            function(token)
            {
                //alert(token);
                //callback when pushwoosh is ready
                $.jStorage.set("pushToken", token);
                onPushwooshAndroidInitialized(token);
            },
            function(status)
            {
                //alert("failed to register: " +  status);
                console.warn(JSON.stringify(['failed to register ', status]));
            }
        );

    } else { //Push are disabled in user settings
        //unregister for push notifications
        pushNotification.unregisterDevice(
            function (token) {
                console.log("unregisterDevice, token: " + token);
                //alert("unregisterDevice, token: " + token);
            },

            function (status) {
                console.warn("registerDevice failed, status:" + status);
                //alert("registerDevice failed, status:" + status);
            }
        );
    }

}

function onPushwooshAndroidInitialized(pushToken)
{
    //output the token to the console
    console.warn('push token: ' + pushToken);

    var pushNotification = window.plugins.pushNotification;

    //if you need push token at a later time you can always get it from Pushwoosh plugin
/*    pushNotification.getPushToken(
        function(token)
        {
            console.warn('push token: ' + token);
        }
    );*/

    //and HWID if you want to communicate with Pushwoosh API
/*    pushNotification.getPushwooshHWID(
        function(token) {
            console.warn('Pushwoosh HWID: ' + token);
        }
    );*/

    pushNotification.getTags(
        function(tags)
        {
            //alert(JSON.stringify(tags));
            console.warn('tags for the device: ' + JSON.stringify(tags));
        },
        function(error)
        {
            console.warn('get tags error: ' + JSON.stringify(error));
        }
    );


    //set multi notificaiton mode
    pushNotification.setMultiNotificationMode();
    pushNotification.setEnableLED(true);

    //set single notification mode
    //pushNotification.setSingleNotificationMode();

    //disable sound and vibration
    //pushNotification.setSoundType(1);
    //pushNotification.setVibrateType(1);

    pushNotification.setLightScreenOnNotification(false);

    //goal with count
    //pushNotification.sendGoalAchieved({goal:'purchase', count:3});

    //goal with no count
    //pushNotification.sendGoalAchieved({goal:'registration'});

    //setting list tags
    //pushNotification.setTags({"MyTag":["hello", "world"]});

    //settings tags
    pushNotification.setTags({deviceName:"hello", deviceId:10},
        function(status) {
            console.warn('setTags success');
        },
        function(status) {
            console.warn('setTags failed');
        }
    );

    //Pushwoosh Android specific method that cares for the battery
    //pushNotification.startGeoPushes();
}


/***************************************
 ************ iOS Setup ****************
 ***************************************/



function registerPushwooshIOS() {
    var pushNotification = window.plugins.pushNotification;

    //set push notification callback before we initialize the plugin
    document.addEventListener('push-notification',
        function(event)
        {
/*          notification: Object
                aps: Object
                    alert: "Testing"
                    badge: 0
                    sound: "default"
                    __proto__: —
                onStart: false
                p: "A"
            __proto__: —*/

            //get the notification payload
            var notification = event.notification;

            //display alert to the user for example
            //alert(notification.aps.alert);
            myApp.addNotification({
                title: "CRI Mobile Push",
                message: notification.aps.alert,
                media: '<i class="icon icon-cri-leaf"></i>'
            });
            //to view full push payload
            //myApp.alert(JSON.stringify(notification));

            //clear the app badge
            pushNotification.setApplicationIconBadgeNumber(0);
        }
    );

    //initialize the plugin
    pushNotification.onDeviceReady({pw_appid:"FA47B-C5156"});

    if(storage.getPushState()){

        //register for pushes
        pushNotification.registerDevice(
            function(status)
            {
                var deviceToken = status['deviceToken'];
                console.warn('registerDevice: ' + deviceToken);
                $.jStorage.set("pushToken", deviceToken);
                onPushwooshiOSInitialized(deviceToken);
            },
            function(status)
            {
                console.warn('failed to register : ' + JSON.stringify(status));
                //alert(JSON.stringify(['failed to register ', status]));
            }
        );

        //reset badges on start
        pushNotification.setApplicationIconBadgeNumber(0);

    } else {

        //unregister for push notifications
        pushNotification.unregisterDevice(
            function (token) {
                console.log("unregisterDevice, token: " + token);
                //alert("unregisterDevice, token: " + token);
            },

            function (status) {
                console.warn("registerDevice failed, status:" + status);
                //alert("registerDevice failed, status:" + status);
            }
        );

    }

}

function onPushwooshiOSInitialized(pushToken)
{
    var pushNotification = window.plugins.pushNotification;
    //retrieve the tags for the device
    pushNotification.getTags(
        function(tags) {
            console.warn('tags for the device: ' + JSON.stringify(tags));
        },
        function(error) {
            console.warn('get tags error: ' + JSON.stringify(error));
        }
    );

    //example how to get push token at a later time
/*    pushNotification.getPushToken(
        function(token)
        {
            console.warn('push token device: ' + token);
        }
    );*/

    //example how to get Pushwoosh HWID to communicate with Pushwoosh API
    pushNotification.getPushwooshHWID(
        function(token) {
            console.warn('Pushwoosh HWID: ' + token);
        }
    );

    //start geo tracking.
    //pushNotification.startLocationTracking();
}